# Assorted Anki 2.1 Plugins

Various assorted Anki 2.1 Plugins

# COPYRIGHT
As this is a collection of projects rather than a single project, all plugins are licensed seperately.
PassFail is licensed under the GPL 3.0 license.
"next due" is licensed under the MIT license.
